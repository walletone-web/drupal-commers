<?php
namespace WalletOne\Classes\ExtraMethods;

class W1PaymentsDrupal7Commerce extends \WalletOne\Classes\StandardsMethods\W1Payments  {
  public $action;
      
  function __construct($config, $params = [], $nameCms = '') {
    parent:: __construct($config, $params, $nameCms);
    
    $this->logger = \Logger::getLogger(__CLASS__);
  }
  
  /**
   * Creating an array with the data for payment.
   * 
   * @param array $settings
   *  Settings from module.
   * @param array $invoce
   *  Order data.
   * @return array
   */
  public function createFormArray($settings, $invoce){
    $fields = [];
    $fields['CMS'] = [
      '#type' => 'hidden',
      '#value' => $this->numCms,
    ];
    $fields['WMI_CURRENCY_ID'] = array(
      '#type' => 'hidden',
      '#value' => $invoce->currencyId
    );
    $fields['WMI_DESCRIPTION'] = [
      '#type' => 'hidden',
      '#value' => "BASE64:" . base64_encode(sprintf(w1OrderDescr, $invoce->orderId, $this->siteName)),
    ];
    $fields['WMI_EXPIRED_DATE'] = [
      '#type' => 'hidden',
      '#value' => date('Y-m-d\TH:i:s', time() + 2592000),
    ];
    $fields['WMI_FAIL_URL'] = [
      '#type' => 'hidden',
      '#value' => $this->failUrl,
    ];
    $fields['WMI_MERCHANT_ID'] = [
      '#type' => 'hidden',
      '#value' => $settings->merchantId,
    ];
    $fields['WMI_PAYMENT_AMOUNT'] = [
      '#type' => 'hidden',
      '#value' => number_format($invoce->summ, 2, '.', ''),
    ];
    
    $orderId = $invoce->orderId . (preg_match('/.cms$/ui', $_SERVER['HTTP_HOST']) !== false 
          || preg_match('/.walletone.com$/ui', $_SERVER['HTTP_HOST']) !== false ? '_'.$_SERVER['HTTP_HOST'] : '');
    $fields['WMI_PAYMENT_NO'] = [
      '#type' => 'hidden',
      '#value' => $orderId,
    ];
    $fields['WMI_SUCCESS_URL'] = [
      '#type' => 'hidden',
      '#value' => $this->successUrl,
    ];
    $fields['WMI_SIGNATURE'] = [
      '#type' => 'hidden',
      '#value' => $settings->signature,
    ];
    $fields['WMI_CULTURE_ID'] = [
      '#type' => 'hidden',
      '#value' => $settings->cultureId,
    ];
	$fields["WMI_CUSTOMER_EMAIL"] = [
      '#type' => 'hidden',
      '#value' => $invoce->emailBuyer
    ];
    $fields['#action'] = $this->action;
    $fields['submit'] = [
      '#type' => 'submit',
      '#value' => w1OrderSubmitShort,
    ];

    $taxes = [
        'tax_ru_1' => 'round(%.2f * 0 / 100,2)',
        'tax_ru_2' => 'round(%.2f * 0 / 100,2)',
        'tax_ru_3' => 'round(%.2f * 10 / 100,2)',
        'tax_ru_4' => 'round(%.2f * 18 / 100,2)',
        'tax_ru_5' => 'round(%.2f * 10 / 110,2)',
        'tax_ru_6' => 'round(%.2f * 18 / 118,2)',
    ];
    $tax_formula = isset($taxes[$settings->taxType]) ? $taxes[$settings->taxType] : '0';

    $products = array();
    $order_wrap = entity_metadata_wrapper('commerce_order', intval($invoce->orderId));
    $line_item_types = commerce_product_line_item_types();
    
    foreach ($order_wrap->commerce_line_items as $line_item) {
      if (in_array($line_item->type->value(), $line_item_types)) {
		$products[] = array(
            "Title" => $line_item->commerce_product->title->value(),
            "Quantity" => $line_item->quantity->value(),
            "UnitPrice" => number_format($line_item->commerce_unit_price->amount_decimal->value(),2,".",""),
            "SubTotal" => number_format($line_item->quantity->value() * $line_item->commerce_unit_price->amount_decimal->value(), 2,".",""),
            "TaxType" => $settings->taxType,
            "Tax" => eval('return '. sprintf($tax_formula, (float)($line_item->quantity->value() * $line_item->commerce_unit_price->amount_decimal->value())) .';'),
        );
      }
    }

    $fields["WMI_ORDER_ITEMS"] = [
        '#type' => 'hidden',
        '#value' => json_encode($products)
    ];
	
    $fields['#after_build'] = ['w1_drupal_commerce_redirect_form_clear'];
    return $fields;
  }
  
}
