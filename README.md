# README #
This module is designed to receive payment orders for CMS Drupal module Commerce

=== Wallet One Payment Commerce ===
Contributors: WalletOne
Version: 3.2.2
Requires at least: 4.30
Tested up to: 4.53
Stable tag: 4.53
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One module is a payment system for CMS Drupal.

== Description ==
If you have an online store, then you need a module payments for orders made.

== Installation ==
1. Register on the site http://www.walletone.com
2. Download the module files on the site.
3. Instructions for configuring the plugin is in the file read.pdf.

== Screenshots ==
Screenshots are to be installed in the file read.pdf

== Changelog ==
= 1.1.1 =
* Fix - Adding translation for english version

= 1.1.2 =
* Fix - Fixed bug with success url

= 1.1.3 =
* Fix - Fixed bug with return answer for payment system

= 1.1.4 =
* Fix - Fixed bug with return answer for payment system

= 1.1.5 =
* Fix - Added answer for payment system

= 2.0.0 =
* Fix - The added a new universal components and class

= 2.0.1 =
* Fix - Fixed created icon and add new settings

= 2.0.2 =
* Fix - Fixed bug with generated token for only autorization user

= 2.1.0 =
* Fixed bug with checkin signature in codding string windows-1251

= 3.0.0 =
* Changed the structure of the plugin.
* Changed a handler of return at the payment system

= 3.1.0 =
* Add fields: Email, Products. Change expired date for invoice.

= 3.2.0 =
* Add field "Tax type" in settings.

= 3.2.1 =
* Fixed tax type title

= 3.2.2 =
* Fix - add title in product

== Frequently Asked Questions ==
No recent asked questions
